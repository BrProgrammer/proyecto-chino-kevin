package intriago.mora.kevin.farmaciasytiendas.Screens;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.codesgood.views.JustifiedTextView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import intriago.mora.kevin.farmaciasytiendas.Model.Productos;
import intriago.mora.kevin.farmaciasytiendas.R;

public class DetalleProductoActivity extends AppCompatActivity {

    private ImageView imageView;
    private TextView nombre, precio;
    private JustifiedTextView detalle;
    private String lista = "", farmacia = "", id= "";
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_producto);

        imageView = (ImageView)findViewById(R.id.ImagenProductoFT);
        nombre = (TextView)findViewById(R.id.LBLNombrePRoducto);
        precio = (TextView)findViewById(R.id.LBLPrecio);
        detalle = (JustifiedTextView) findViewById(R.id.LBLJustificado);
        firebaseDatabase = FirebaseDatabase.getInstance();

        if (getIntent() != null){
            lista = getIntent().getStringExtra("lista");
            farmacia = getIntent().getStringExtra("farmacia");
            id = getIntent().getStringExtra("id");
        }
        if(!lista.isEmpty() && lista != null && !farmacia.isEmpty() && farmacia != null){
            databaseReference = firebaseDatabase.getReference().child("TIENDAS Y FARMACIAS").child(lista).child(lista).child(farmacia).child(farmacia);
            Lista();
        }
    }

    private void Lista() {

        databaseReference.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Productos productos =dataSnapshot.getValue(Productos.class);
                Picasso.get().load(productos.getFoto()).into(imageView);
                nombre.setText(productos.getNombre());
                precio.setText("$" + productos.getPrecio());
                detalle.setText(productos.getDetalle());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
