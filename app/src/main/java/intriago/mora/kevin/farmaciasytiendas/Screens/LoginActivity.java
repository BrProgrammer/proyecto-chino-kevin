package intriago.mora.kevin.farmaciasytiendas.Screens;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import intriago.mora.kevin.farmaciasytiendas.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText usuario, contraseña;
    private Button iniciar,registrarse;
    private ViewFlipper viewFlipper;
    private FirebaseAuth firebaseAuth;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        usuario = (EditText)findViewById(R.id.TXTUser);
        contraseña = (EditText)findViewById(R.id.TXTPass);
        iniciar = (Button)findViewById(R.id.BTNIniciarSesion);
        registrarse = (Button)findViewById(R.id.BTNRegistro);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        viewFlipper = (ViewFlipper)findViewById(R.id.ViewFliper);
        iniciar.setOnClickListener(this);
        registrarse.setOnClickListener(this);

        int imagenes[] = {R.drawable.sapito, R.drawable.tiendabarrio, R.drawable.logocruz, R.drawable.tiend};
        for (int i = 0; i< imagenes.length; i++){
            fliper(imagenes[i]);
        }

    }

    private void fliper(int image) {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.BTNIniciarSesion:
                if (usuario.getText().toString().isEmpty()) {
                    usuario.setError(getResources().getString(R.string.error));
                } else if (contraseña.getText().toString().isEmpty()) {
                    contraseña.setError(getResources().getString(R.string.error));
                } else {
                    if (usuario.getText().toString().isEmpty()) {
                        usuario.setError(getResources().getString(R.string.error));
                    } else if (contraseña.getText().toString().isEmpty()) {
                        contraseña.setError(getResources().getString(R.string.error));
                    } else {
                        progressDialog.setMessage("ACCEDIENDO..!");
                        progressDialog.show();
                        firebaseAuth.signInWithEmailAndPassword(usuario.getText().toString().trim(), contraseña.getText().toString().trim())
                                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                            @Override
                            public void onSuccess(AuthResult authResult) {
                                startActivity(new Intent(LoginActivity.this, Home.class));
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(LoginActivity.this, "ERROR", Toast.LENGTH_LONG).show();
                                progressDialog.hide();
                            }
                        });
                    }
                }
                break;

            case R.id.BTNRegistro:
                startActivity(new Intent(this, RegistroUsuarioActivity.class));
                break;
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        if (currentUser!=null){
            startActivity(new Intent(LoginActivity.this, Home.class));
            finish();
        }else {
            //necesita iniciar sesion
        }
    }
}
