package intriago.mora.kevin.farmaciasytiendas.Screens;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import intriago.mora.kevin.farmaciasytiendas.Model.Usuarios;
import intriago.mora.kevin.farmaciasytiendas.R;

public class RegistroUsuarioActivity extends AppCompatActivity implements View.OnClickListener {

    private ViewFlipper viewFlipper;
    private ProgressDialog progressDialog;
    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference users;
    private EditText nombre, apellido, cedula, celular, correo, clave, vclave, ubicacion;
    private Button registrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuario);
        nombre =(EditText)findViewById(R.id.TXTNombres);
        apellido = (EditText)findViewById(R.id.TXTApellidos);
        cedula = (EditText)findViewById(R.id.TXTCedula);
        celular = (EditText)findViewById(R.id.TXTCelular);
        correo = (EditText)findViewById(R.id.TXTCorreo);
        clave = (EditText)findViewById(R.id.TXTClave);
        vclave = (EditText)findViewById(R.id.TXTVClave);
        ubicacion = (EditText)findViewById(R.id.TXTUbicacion);
        viewFlipper = (ViewFlipper)findViewById(R.id.ViewFliper2);
        registrar = (Button)findViewById(R.id.BTNRegistrar);
        progressDialog = new ProgressDialog(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseAuth = FirebaseAuth.getInstance();
        users = firebaseDatabase.getReference("USUARIOS");
        registrar.setOnClickListener(this);

        int imagenes[] = {R.drawable.sapito, R.drawable.tiendabarrio, R.drawable.logocruz, R.drawable.tiend};
        for (int i = 0; i< imagenes.length; i++){
            fliper(imagenes[i]);
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
        } else {
            locationStart();
        }
    }

    private void locationStart() {
        LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Localizacion Local = new Localizacion();
        Local.setMainActivity(this);
        final boolean gpsEnabled = mlocManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (!gpsEnabled) {
            Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(settingsIntent);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,}, 1000);
            return;
        }
        mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, (LocationListener) Local);
        mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) Local);

    }

    private void fliper(int image) {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(2000);
        viewFlipper.setAutoStart(true);
        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.BTNRegistrar:
                if (nombre.getText().toString().isEmpty()){
                    nombre.setError(getString(R.string.error));
                    nombre.requestFocus();
                }else if (apellido.getText().toString().isEmpty()){
                    apellido.setError(getString(R.string.error));
                    apellido.requestFocus();
                }else if (cedula.getText().toString().isEmpty()){
                    cedula.setError(getString(R.string.error));
                    cedula.requestFocus();
                }else if (celular.getText().toString().isEmpty()){
                    celular.setError(getString(R.string.error));
                    celular.requestFocus();
                }else if (correo.getText().toString().isEmpty()){
                    correo.setError(getString(R.string.error));
                    correo.requestFocus();
                }else if (clave.getText().toString().isEmpty()){
                    clave.setError(getString(R.string.error));
                    clave.requestFocus();
                }else if (vclave.getText().toString().isEmpty()) {
                    vclave.setError(getString(R.string.error));
                    vclave.requestFocus();
                }else if (CedulaCorrecta(cedula.getText().toString())==false) {
                    cedula.setError(getString(R.string.cedulacorrecta));
                    cedula.setText("");
                    cedula.requestFocus();
                }else if (validar_clave(clave.getText().toString())==true){
                    clave.setError(getString(R.string.clavevalidar));
                    clave.requestFocus();
                }else if(vclave.getText().toString().equals(clave.getText().toString())==false) {
                    vclave.setError(getString(R.string.comprobarclave) + " " + getString(R.string.password) + " " + getString(R.string.xxx));
                    vclave.requestFocus();
                }else if (validate_correo(correo.getText().toString())==false){
                    correo.setError(getString(R.string.correovalido));
                    correo.requestFocus();
                }else {
                    progressDialog.setMessage("REGISTRANDO USUARIO..!");
                    progressDialog.show();
                    firebaseAuth.createUserWithEmailAndPassword(correo.getText().toString(), clave.getText().toString())
                            .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                                @Override
                                public void onSuccess(AuthResult authResult){
                                    Usuarios usuario = new Usuarios();
                                    usuario.setNombres(nombre.getText().toString());
                                    usuario.setApellidos(apellido.getText().toString());
                                    usuario.setCedula(cedula.getText().toString());
                                    usuario.setCelular(celular.getText().toString());
                                    usuario.setUbicacion(ubicacion.getText().toString());
                                    usuario.setCorreo(correo.getText().toString());

                                    users.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(usuario).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            firebaseAuth.signOut();
                                            startActivity(new Intent(RegistroUsuarioActivity.this, LoginActivity.class));
                                            finish();
                                        }
                                    }).addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(RegistroUsuarioActivity.this, "ERROR AL REGISTRAR USUARIO..!", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                        }
                    });
                }
            break;
        }

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 1000) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                locationStart();
                return;
            }
        }
    }

    public void setLocation(Location loc) {
        //Obtener la direccion de la calle a partir de la latitud y la longitud
        if (loc.getLatitude() != 0.0 && loc.getLongitude() != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(
                        loc.getLatitude(), loc.getLongitude(), 1);
                if (!list.isEmpty()) {
                    Address DirCalle = list.get(0);
                    ubicacion.setText(DirCalle.getAddressLine(0));
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean CedulaCorrecta(String cedula) {
        boolean cedulaCorrecta = false;
        try {

            if (cedula.length() == 10)
            {
                int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
                if (tercerDigito < 6) {
                    int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
                    int verificador = Integer.parseInt(cedula.substring(9,10));
                    int suma = 0;
                    int digito = 0;
                    for (int i = 0; i < (cedula.length() - 1); i++) {
                        digito = Integer.parseInt(cedula.substring(i, i + 1))* coefValCedula[i];
                        suma += ((digito % 10) + (digito / 10));
                    }

                    if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                        cedulaCorrecta = true;
                    }
                    else if ((10 - (suma % 10)) == verificador) {
                        cedulaCorrecta = true;
                    } else {
                        cedulaCorrecta = false;
                    }
                } else {
                    cedulaCorrecta = false;
                }
            } else {
                cedulaCorrecta = false;
            }
        } catch (NumberFormatException nfe) {
            cedulaCorrecta = false;
        } catch (Exception err) {
            cedulaCorrecta = false;
        }

        if (!cedulaCorrecta) {
        }
        return cedulaCorrecta;
    }

    public class Localizacion implements LocationListener {
        RegistroUsuarioActivity usuarioNuevo;

        public void setMainActivity(RegistroUsuarioActivity usuarioNuevo) {
            this.usuarioNuevo = usuarioNuevo;
        }

        @Override
        public void onLocationChanged(Location loc) {
            // Este metodo se ejecuta cada vez que el GPS recibe nuevas coordenadas
            // debido a la deteccion de un cambio de ubicacion

            loc.getLatitude();
            loc.getLongitude();
            this.usuarioNuevo.setLocation(loc);
        }

        @Override
        public void onProviderDisabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es desactivado
            Toast.makeText(RegistroUsuarioActivity.this, "GPS DESACTIVADO", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Este metodo se ejecuta cuando el GPS es activado
            Toast.makeText(RegistroUsuarioActivity.this, "GPS ACTIVADO", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Log.d("debug", "LocationProvider.AVAILABLE");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Log.d("debug", "LocationProvider.OUT_OF_SERVICE");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Log.d("debug", "LocationProvider.TEMPORARILY_UNAVAILABLE");
                    break;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }

    public boolean validar_clave(String password){
        String PASSWORD_PATTERN ="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\\\S+$).{8,}$";
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public boolean validate_correo(String email){
        String EMAIL_ADDRESS ="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern1 = Pattern.compile(EMAIL_ADDRESS);
        Matcher matcher1 = pattern1.matcher(email);
        return matcher1.matches();
    }
}
