package intriago.mora.kevin.farmaciasytiendas.Screens;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import intriago.mora.kevin.farmaciasytiendas.Holder.HolderLista;
import intriago.mora.kevin.farmaciasytiendas.Interface.ItemClickListener;
import intriago.mora.kevin.farmaciasytiendas.Model.Productos;
import intriago.mora.kevin.farmaciasytiendas.R;

public class ProductosDeTiendaFarmacia extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private DatabaseReference databaseReference;
    private FirebaseDatabase firebaseDatabase;
    private String lista = "", farmacia = "";

    private FirebaseRecyclerAdapter<Productos, HolderLista> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos_de_tienda_farmacia);
        firebaseDatabase = FirebaseDatabase.getInstance();
        recyclerView = findViewById(R.id.recycler_producto);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        if (getIntent() != null){
            lista = getIntent().getStringExtra("lista");
            farmacia = getIntent().getStringExtra("farmacia");
        }
        if(!lista.isEmpty() && lista != null && !farmacia.isEmpty() && farmacia != null){
            databaseReference = firebaseDatabase.getReference().child("TIENDAS Y FARMACIAS").child(lista).child(lista).child(farmacia).child(farmacia);
            Lista();
        }
    }

    private void Lista() {
        adapter = new FirebaseRecyclerAdapter<Productos, HolderLista>
                (Productos.class,
                        R.layout.layout_farmacias_y_tiendas,
                        HolderLista.class,
                        databaseReference){
            @Override
            protected void populateViewHolder(HolderLista viewHolder, Productos model, int position) {

                viewHolder.txtMenuName.setText(model.getNombre());
                Picasso.get().load(model.getFoto()).into(viewHolder.imageView);
                viewHolder.setItemClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent intent = new Intent(ProductosDeTiendaFarmacia.this, DetalleProductoActivity.class);
                        intent.putExtra("lista", lista);
                        intent.putExtra("farmacia", farmacia);
                        intent.putExtra("id", adapter.getRef(position).getKey());
                        startActivity(intent);
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }
}
