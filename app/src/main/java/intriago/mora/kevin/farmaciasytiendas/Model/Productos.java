package intriago.mora.kevin.farmaciasytiendas.Model;

public class Productos {
    private String Nombre, Foto, Precio, Detalle;

    public Productos() {
    }

    public Productos(String nombre, String foto, String precio, String detalle) {
        Nombre = nombre;
        Foto = foto;
        Precio = precio;
        Detalle = detalle;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String precio) {
        Precio = precio;
    }

    public String getDetalle() {
        return Detalle;
    }

    public void setDetalle(String detalle) {
        Detalle = detalle;
    }
}
